package cz.mirekzamecnik.mirahomebe.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class IndexController {

    private static final String UPSTREAM_SERVICE = "/name";

    @GetMapping("/")
    public String index(){
        return "Welcome to Mira home";
    }

    @GetMapping("/integrate")
    public String integrate(){

        RestTemplate client = new RestTemplate();
        return "I am integrated with" + client.getForObject(UPSTREAM_SERVICE, String.class);
    }
}
