package cz.mirekzamecnik.mirahomebe.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import cz.mirekzamecnik.mirahomebe.dto.HealtcheckDTO;

@RestController
public class HealtcheckController {

    @GetMapping("/health")
    public HealtcheckDTO check() {
        return new HealtcheckDTO();
    }

}
