package cz.mirekzamecnik.mirahomebe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MirahomeBeApplication {

	public static void main(String[] args) {
		SpringApplication.run(MirahomeBeApplication.class, args);
	}
}
