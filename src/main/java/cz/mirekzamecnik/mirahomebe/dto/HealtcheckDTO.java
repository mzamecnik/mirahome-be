package cz.mirekzamecnik.mirahomebe.dto;

public class HealtcheckDTO {

    private String status = "OK";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
