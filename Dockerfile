FROM openjdk:8-jdk-alpine
LABEL maintainer="mirekzamecnik.cz" 

ADD ./target/mirahome-be-0.0.1-SNAPSHOT.jar /usr/local/mirahome-be.jar
WORKDIR /usr/src/mirahome-be

EXPOSE 8080
CMD ["java", "-jar", "/usr/local/mirahome-be.jar"]